# LAND - FOOD - WATER - SHELTER

## THE KINDOM MUDROOM

Land, food, water and shelter are the basic pre-requisites for life that were once freely available to our ancestors all across earth. Increasingly, access to land, food, water and shelter is being held to ransom, available only to those who give up a lifetime of labour just to be able to lay claim to a small plot of land as "tenants" on land title deeds, even though it is still only borrowed as it is encumbered with contracts, values, taxes, mortgages and duties and cannot be freely passed down to children and grandchildren.

This situation has led to the vast majority of men, women and children all across earth becoming refugees on their motherlands, separated from their lands and lineages, with middle men and women dictating their relationship to land, food, water and shelter based on an imposed value of economic worth that has nothing to do with a moral compass or conscience.

<!--more-->
The resulting chaos, confusion, unhappiness, despair, rage, hatred, greed and selfishness coming from a world of men, women and children surviving the scarcity of an economic system that very few can conquer has directed us onto our present, collective path of almost total annihilation of everything of life, the natural world that is the land, food, water and shelter.

We have sat back and watched, distracted by the empty trinkets of a commercial world, as that which we need to live has been fenced off, turned into inhospitable concrete jungles, burnt, razed, drained, starved, parched, raped and destroyed.

We are all fully responsible for supporting and maintaining the systems that continue that devastation and destruction.

To turn back the tide of this decimation, every man, woman and child needs free access to land, food, water and shelter to live their lives as caretakers of all of life, learning to let go of the habits of destruction and form the habits of conservation, regeneration, responsibility, community and do-no-harm.

## OUR VISION

To create a large community of men, women and children who are committed to learning to live without doing harm to nature, gradually withdrawing from our harmful habits as we examine, challenge and change them, all the while creating abundant food forests and the sustainable infrastructure necessary to transition the land and our lives away from being answerable to the dictates of other men and women acting as third parties between us and land, us and nature, us and the full responsibility for our lives on the land. We call this the Kindom Mudroom, a domain of Kin where we wash off the "mud" of The System.

The land belongs to no one but is taken care of by everyone. The best spots are for all to enjoy and we focus our energy on the regeneration of land that has been damaged by logging, agriculture, animal husbandry and poor management, until it is once again a living paradise of abundance and magnificence and a sanctuary to wildlife and MAN.

The community serves as an example of how to live freely without commerce and without harm and will be open to visitors to come and experience the possibilities when land, food, water and shelter are freely available to all to nurture, caretake and regenerate. It provides the inspiration to men, women and children all across earth to take back their full responsibility for land, food, water and shelter and pass it on to the generations to come.

When we are fully responsible for our lives and the lives of those around us, everything is local. We know those producing the food we eat, the tools we use, the houses we live in and the families our children play with; there are no strangers involved and everyone is kept honest and ethical because they are answerable to the whole tribe if they stray.

## Community Action

* The willingness of everyone involved to continually assess their attitudes, actions and e-motional blocks to ensure the successful growth of a do no harm community.
* Creating a paradise with running water throughout the property, growing vast food forests and deeply mineralised organic gardens to provide free food for the local community.
* Sustainable building of a community hub followed by dwellings built using both natural methods - wood, mud, straw, etc. - and recycled metals, old building materials, tyres, bottles, glass, plastic, etc.
* Buying a large building in the local town as a hub for distribution of free food, free workshops on growing and preparing food, natural healing and other aspects of sustainable living with a goal to inspiring networks of people growing and sharing food and other resources to enable a move away from reliance on The System.
* Powered firstly by solar, gas, vegetable oils and fire, we will gradually learn other methods of food preparation, keeping warm and working with hand tools to learn traditional crafts.
* Natural food storage of crops built into hillsides or underground for natural food preservation providing all-year-round sustainability.
* A mostly raw (where possible), mostly vegan diet for optimum health and sustainability and to stop the slaughter of traumatised animals for our table.
* As the community, infrastructure and skill sets grow, we offer sanctuary to those suffering under their refugee status, all free of charge.
* An Art-house Studio creates contemporary music, documentaries, humorous sketches and cartoons and a presence on social media platforms to draw people to the do no harm message we live by.
* Services and produce are offered for free but donations and gifts are accepted - whether for talks, workshops, food, music, healing, etc.
* Possible financial ventures include marketing T-shirts with appropriate slogans and healthy, vegan food stalls at markets and festivals designed to promote the culture of Kindom and inspire gifts and participation.

# Our Skills and Experience

* Growing an abundant food forest garden on 1/16th of an acre of  a small, suburban, rental property producing around 2 tons of food every year.
* Many different people coming to work in the garden, to talk and learn, giving us the opportunity to observe the barriers to a successful community and what it will take for each of us to overcome them.
* Many original songs ready to be taken to a professional level and previous experience creating bands and events.
* A thorough comprehension of the e-motional rackets, games, power trips and self-obsession preventing most men and women from creating their heartfelt dreams.
* Twenty years in a self-made business in a service industry employing hundreds of staff giving us the ability to manage finances, forward plan, run marketing campaigns and the big picture goals that drive the small decisions. Experience with marketing, public relations, entertainment and leisure industries since the late 70s.
* Over 50 years of extensive research with hands on experience to determine exactly where the problems lie and what to do about it. Building a diverse research library in a free website that had over 440 million unique IP addresses hitting the home page.
* Our work towards this goal has been supported by many over the years, financially, materially and physically. To sustain the community as it grows towards true freedom on the land, some continue working to earn money as others create the infrastructure and food forest for them to live in as they are working until the time comes for them to stop working outside the community as well because the community is able to exist independently from The System or because a large gift enables everyone to put their energy into working on the land.

After 17 years of scrutinising information to work out why we all persist in harming our lives and all of life around us, observing each other and the men and women who have come into our lives, learning how to maintain true health, simplifying food growing practices and researching how to live unplugged from system infrastructure, we are ready to get on with it, along with many brothers and sisters who share the same inspiration. We now have the start up land, 27 acres, at Temagog, 25 minutes west of Kempsey, NSW, Australia, and we are willing and eager to spend the rest of our lives recreating paradise.

<div style="text-align: center">
<h2>What else is there to do?</h2>

Why is it so vital that we remove our lives from The System?<br /><br />

Why can't we just learn to live sustainable lives within the current structure of finance, government, religion, education?<br /><br />

Because it is impossible to live within The System and NOT<br />
cause constant catastrophic harm to life.<br /><br />

The System is built out of tampering with life<br />
and cannot exist without harm to life.<br /><br />
</div>

The System is the world's biggest garbage dump, an artificial, alien world of catacombs constructed with destroyed life and all the human intelligence living in it has been assimilated into a one track, hive mind collective of consumer drones programmed through materialism to keep destroying life and adding it to the one world garbage dump.

As we still have free will and focus, we have been working on breaking our ties to the social engineering of garbage hive mind lifestyles and have started the Kindom Mudroom to disentangle our deeply compromised lives from the wreckage of endless destruction to life and, in spite of still being compromised hypocrites, to offer the opportunity of a vision for others to also focus on nurturing life all around and nothing else so that they disentangle their real lives from the hive mind collective, if they are inspired to do so (we cannot do it for them).

<div style="text-align: center">How do we exchange a lifetime of wreckage<br />
for a lifetime of co-creation with life?</div>

By changing the programming of the robotic mindset that keeps us assimilated into the hive, the in-divide-you-all-ism of a spoon-fed slave keeping us bound to the synthetic strands of fake security supplied by multi-national corporations and an imaginary sense of belonging to the alien world where nothing is local, with a programming that will always have us go in the opposite direction to the dreams of our hearts of nurturing life all around, as we fight for the implanted opinions, views, beliefs, faiths & feelings of an imaginary self amidst a glorious, gold-plated wonderland that keeps us in the e-motional hive mind collective of doubt, uncertainty, distrust, suspicion, fear, etc, even as we think that we are walking away from this dark hive mind, which has NOTHING to do with the heart in all of life.

No matter the programming of a drone, however beautiful their downloaded beliefs may be, when they reach their use by date there is always another drone to take their place and nothing has changed in the here & now. The alien world is intact.

Instead of being bees creating honeycombs of natural life to nurture all of life, we are now "good hearted", "feel good", "green", alien drones creating artificial honeycombs of garbage that inter-connectively form the rotting, putrid, one world garbage heap that has arisen out of the Industrial Revolution, the printing press,  education, TV, advertising, internet, radio, engineering, technology, telecommunication, motorised transport, suburbs, cities, religions, philosophies, etc.

We can watch all the best documentaries and current affairs, study under all the great minds covering all fields of endeavour, sit by the side of all the great intellectuals, pore over all the great books of literature and divine inspirations, follow messiahs and saviours, dive into occults and esoterics, VISIT the wisest psychologists and counsellors and yet, with all the brilliant collective minds that have brought forth the progress of civilization with all its inventions, JUST LOOK AT THE MESS WE ARE ALL IN RIGHT NOW ALL ACROSS EARTH.

All the "I love you", romance, "I care", poetry, feel good whatever rhetoric, consciousness, God, intervention, Christ, other worldly whatever, channelling & freedom through the law Information has only supported the expansion of the hive mind collective & none of it has ever stopped it in its tracks.

All the multi-hundreds of generations of parents loving their children has never stopped this one world garbage heap from growing bigger and bigger. Instead, everything of The System that we turn to looking for answers has only supported the evolution of The System, the one world garbage heap.

## What is the common denominator?

The common denominator is how each of us in The System is thinking, feeling and living, instant by instant by instant; this is what is causing the ongoing tampering with life which causes destruction. Everything of nature/reality/earth has an original purpose and we are all now paying the price for messing with the natural purpose of all of life, both minuscule and giant, generation after generation after generation.

We were never meant to interfere with ANYTHING of life, including each other, or to corrupt, distort or confuse its natural purpose. Life comes out of life comes out of life comes out of life; this is the original purpose of life and is what sustains life.

We don't have to teach a puppy how to be a dog but we have made such a mess of life that the whole of life is weakened, sickened, destroyed and altered. We have all been tampered with to the point that we have no respect for life anymore and think it is normal to tamper with the natural purpose of life and place our good heart with the actions of destroying life with happy smiles on our faces (most of the time).

The whole of "The System" is the result of interference with the natural purpose of life and we are fully  responsible for the continuation of The System day after day after day, but very few amongst us are willing to look at the consequences of our daily actions. We have created civilisation after civilisation after civilisation that tampers with life and they have all become extinct, just as this one will become extinct shortly.
 
Interfere with the innocence of a child and that child can grow to harm millions of men, women and children. Harm the air and many breathing the air will be deeply affected. Harm the water and uncountable life forms will suffer. Tamper with a forest and vast arrays of species will die in that forest, many becoming extinct. Damage the earth and the whole of earth will eventually be destroyed. What we do wherever we are that supports and causes the harm of tampering with life is affecting men, women and children all across earth no matter where they may be.

We are all intrinsically connected with life but we have all been so severely interfered with for so long since childhood that we have lost connection with the big picture of reality and the full responsibility that comes with doing no harm to what we are all naturally part of.

All children from the womb should be raised by nature without anyone inside their heads defining them and disconnecting them from their true purpose in/as nature. In The System, children are being defined before they are even conceived and in the womb and then they are bombarded with system knowledge from the womb to the grave. We now live in the end times of this massive fall from reality (LIFE) and in the coming few years we will witness the escalation of children dying before the age of 6, 2/10 then 4/10 then 6/10 then 8/10 and then earth wide extinction to civilisation, again.

## Just One Screw.........

Our disconnection from all of life is disconnection from the whole which means that we have got used to compartmentalising everything. Thus when we buy a screw, we don't think we are doing harm....... just one screw, how bad can that be? Well, when we explode the background of the screw, we can see that the production of that one screw impacts multi thousands directly and almost everyone indirectly.

The mountains are surveyed and then mined for the metal and the mining industry process involves multi thousands. Then the metal is transported, another industry involving thousands, from drivers through traffic lights and police monitoring to warehouses to aeroplanes, trucks and trains and the thousands involved in the manufacture, sales, repairs, registrations, refuelling of aeroplanes, trucks and trains and the thousands involved in the oil industry and refineries, transport and sales of petrol and diesel, to the factories that produce the screws and the thousands involved in supplying materials and building the factories, including sand mining for concrete, more metal production, the whole electrical and plumbing industries, transportation again, and then those working in the factory making the screws and then the transport comes in again, distribution and all those involved in marketing and wholesale and then all those in the hardware stores selling them and then the car manufacturing industry and all the white collars, blue collars, the bank tellers, the bureaucracy, laws and enforcement as well as building and maintenance of roads through forest and wilderness for us to drive our car to buy the screw and running through all these processes is administration and finance involving banking, accounting, computer hardware and software, printers, photocopiers and all involved in their manufacture, marketing, distribution and sales, and then there are the shops and all the industries involved in building, maintaining and staffing them. And everyone involved need to eat, drink, wee, poo and rest. And none of this happens without governments, local councils, lawyers and lackeys and we all support the government, local councils and everything in between so that we are ALL complicit in the harm done just to be able to buy one screw.

Who are these people involved for us to buy one screw? Who are their families and what are their living conditions? Do they have enough to eat? Do their children have a safe, comfortable bed to sleep in at night? Are the workers having to live away from their families to keep a roof over their heads and food on the table? Are they living under persecution because of their race, religion or interference from colonisers?

Then multiply this scenario to every consumer item we buy. Are we buying anything that has been produced locally? Do we know who has produced it? Do they know us? Are we and our families in their thoughts? Are their families in ours?

We know that our cheap t-shirts were made in sweat shops but we turn a blind eye because we want the cheap t-shirts. We know that many items made in China are made under less than ideal conditions both environmentally and people wise but we still order on ebay and delight in our purchases.

We say we love the mountains but we drive our cars to visit them. We say we love the oceans but we keep using the plastic bags that are clogging them up and killing sea birds and animals. We say we love the rivers but we support industries that fill them with poisons. We say we love the forests but we cut them down to build highways or raise yet more meat. We say we love our children but we feed them toxic food and fill their brains with toxic information and destroy the legacy we are leaving them.

<div style="text-align: center">
We don't want to give up our tampering lifestyles so we look the other way.

<h2>How we have been conned<br />to labour for our daily bread</h2>
</div>

"The System" is a Pax Romanus, a fake peace treaty where all of MAN, all of whom came from invaded, decimated tribes of MAN on land all across earth, have had to survive life apart from their true nature so there will never be a happy ending for anyone trapped in "The System" of fantasy, illusion and delusion.

All the governments, laws, statutes, rules, regulations, etc, ARE the Pax Romanus - the dictates of the conquerors over the conquered. The Pax Romanus is actually a continuation of a war state where all the natural "rights" and the true autonomy of life for every MAN have been abolished.

Every institution and tradition, all the infrastructure and bureaucracy of The System are to coerce and persuade us to keep tampering with life. The social engineering of tampering with life is in the alphabet languages, the mathematics, the geometry and in every aspect and every lifestyle of The System with all its constructs and concepts which is why we are in such a mess despite generations of parents loving their children and wanting the best for them.

We are now living with middle men of the Pax Romanus tampering with the relationships between MAN and land, MAN and Food, MAN and water, MAN and family, MAN and tribe/community, and between MAN and MAN'S consciousness, freedom, truth, peace, joy, abundance and the whole of MAN'S life.

Under this extremely oppressive Pax Romanus, we have to constantly use our fruits of labour inside the middle men's system of power and control to earn their fictitious tokens of commercial values, and then trade our repeatedly taxed fruits of labour (SLAVERY), always under their corporate governance, in every area of our lives so we can all have ongoing access to land, food, water and shelter, which should ALWAYS BE FREE.

The tampering with our commercial values continues to siphon our power so that we never have enough to live beautiful, peaceful lives, instead being subjected to lives of scarcity with the few having all the commercial wealth and the many struggling to make ends meet. The scarcity is so extreme that we all have to turn to tampering, using, abusing, raping, pillaging and plundering each other just to survive, the original purpose of life forgotten.

With the trauma of suffering life vibrating constantly in the background, we turn to the trinkets of materialism and consumerism to numb the pain, seeking pleasure through coffee shops, restaurants, movies, gaming, sport, alcohol, drugs, sex, debauchery, cruelty and more and more tampering to give us some sort of fulfilment but our desires are insatiable and all we are doing is adding to the alien garbage heap and causing more and more destruction to life.

There are industries providing the pleasure and industries repairing the damage caused by the pleasure and all of it adding to the mess.

We can't "own" (not let go of) our last breath, drink or meal without dying. Death (extinction) also spreads when we claim and fight over land, food, water, shelter, air, sunshine, nature, race, culture, creed, title, commercial standing, ethnicity, etc, because as soon as we claim something, we are shrinking the abundance for others and creating exclusion.

Being raised by nature where the true purpose of nature brings forth the good heart of every MAN, there is no need for a Pax Romanus to live under. Expecting indigenous people to fit seamlessly into system life and accept the Pax Romanus that we have all accepted is like asking them to be beamed up to another planet, just as it was to the tribes of MAN on land who are the ancestors of the so-called "non-indigenous" people.

The progress of civilisation that we are all caught up in today is the dying stages of the Pax Romanus that will lead only to the annihilation of everyone trapped in The System underneath this Pax Romanus, this alien world.

The next stage of the evolution of The System is to turn everyone into a smart metre where every detail of their system lives will be in a microchip or some other method of digital recognition containing information. We will no longer be separate from our cash and bank accounts, our credit, our rental contracts, our medical information, our bills, licences and registrations.

Our access to shops, housing, travel and other services will be controlled according to the information contained in our bodies, a tampering so fatal that the last vestiges of freedom will be totally gone. The smart metre can be turned off, by accident or by design and our access to land, food, water, shelter, fuel, children, etc, is cut off. This is the pinnacle of power and control over the useless food eaters, the slaves, the cogs in the wheels of The System.

The Mudroom of Kindom is for those who want to face cold turkey and undo the destructive programming that binds them to being garbologists in their particular hive mind collective while we keep using The System to get out of it, and pass on the willpower, disciplines & focus to children through the example of what it will take to break away from this alien world full of garbage, never to return.

Who can pull their lives out of their MIND of discombobulation that is this mesmerising garbage heap?

Who can stop going "within" to the scripts of annihilation and radiate the virtues of the good heart like the sun radiating its warmth for any life form that needs it, without conditions, contracts or rituals?

The real power is, always has been and always will be with us, the people, BUT we will never break away from our enslavement unless we recognise and undo the programming that maintains our acquiescence to the fake power of the tiny few controlling The System who teach us to tamper with life because this is how they hold on to their power and control. They have no respect for life and they will lie, cheat, deceive, maim and murder to keep the status quo of their power.

We all have lifetimes of practise of living as acquiescent slaves so our default is to fall back on "what are they going to do about it?" instead of asking ourselves, "what are we going to do about it?"

Freedom is full responsibility for our lives and for life all around us so to practise freedom, we have to practise full responsibility for everything we think, feel and do. This means taking full responsibility for the harm that we are causing directly and indirectly and taking steps to keep minimising that harm until harm is no longer part of our everyday lives

We have spent the whole of our lives in The System practising being slaves; learning the full responsibility of freedom will take practise as we undo the habits of slavery and replace them with the habits of true freedom.

Having a good heart is not enough because we are used to using the good heart against the backdrop of The System, not realising that everything we do in The System, despite our good intentions, is only adding to the tampering with life.

What we call security in The System is tenuous at best. Any tweak in the economy, glitch in the oil companies, food or power shortages for whatever reason, draconian laws passed under the excuse of economic collapse, terrorism, pandemics or wars, can upend our so-called security in an instant. True security is a tribe of self-sufficient villagers working for and defending their self-sufficient village.

When there are no more middle men and women between us and land, us and food, us and water, us and shelter, we can take full responsibility for our lives once again and re-learn how to live without tampering with life.

<div style="text-align: center; font-weight: bold;">
When land, food, water and shelter are free, so are we.
</div>
