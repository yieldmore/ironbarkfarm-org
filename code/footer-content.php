  <div class="container">
	<div class="col-12">
		<div class="widget widget_links">
			<h4 class="mb-3 mb-sm-4 nott ls0"><?php echo am_var('name'); ?> - <?php echo am_var('byline'); ?></h4>
            <div>
                Contact:
                <a class="emphasize" href="mailto:<?php echo am_sub_var('contact', 'email'); ?>" target="_blank"><?php echo am_sub_var('contact', 'email'); ?></a> /
                <a class="emphasize" href="<?php echo am_var('url'); ?>donate/" target="_blank">donate</a><br />
                <?php echo am_sub_var('contact', 'donate-details'); ?>
            </div>
		</div>
	</div>


	<div class="header-row justify-content-lg-center header-border top-search-parent">
		<div class="widget widget_links">
			<h4 class="mb-3 mb-sm-4 nott ls0"><?php echo am_var('name'); ?> - <?php echo am_var('byline'); ?></h4>
            <div>
				<ul class="menu-container justify-content-between social-links"><?php foreach(am_var('social') as $item) { ?>
					<a target="_blank" href="<?php echo $item['link']; ?>" title="<?php echo isset($item['name']) ? $item['name'] : $item['type']; ?>" class="<?php echo $item['type']; ?>"><i class="icofont-<?php echo $item['type']; ?>"></i></a><?php } ?>
				</ul>
			</div>
		</div>
	</div>
  </div>

