<?php
DEFINE('FEATUREDPATH', SITEPATH . '/../' . (am_var('local') ? '../development/' : '../'));

function get_featured_pages($plain = true) {
	$featured = get_sheet(FEATUREDPATH . 'featured.tsv', false);

	if (!$plain) return $featured;

	$items = [];
	foreach ($featured->rows as $item)
		$items[] = urlize($item[$featured->columns['title']]);
	return $items;
}

function before_render() {
	$node = am_var('node');
	foreach (am_var('sections') as $slug) {
		$path = am_var('path') . '/content/' . $slug . '/';
		$file = $path . am_var('node');
		if (am_var('node') == $slug) {
			am_var('section', $slug);
			return;
		} else if (disk_file_exists($file . '.md')) {
			am_var('fol', $path);
			am_var('section', $slug);
			am_var('file', $file . '.md');
			return;
		} else if (disk_file_exists($file . '.html')) {
			am_var('html-file', $file . '.html');
			return;
		}
	}

	$sheet = get_featured_pages(false);

	foreach ($sheet->rows as $item) {
 		$slug = urlize($item[$sheet->columns['title']]);
		$file = $item[$sheet->columns['file']];
		if ($slug == am_var('node')) {
			am_var('html-file', FEATUREDPATH . $file . '.html');
			return;
		}
	}
}

function did_render_page() {
	if (am_var('section')) {
		if (am_var('file')) render_txt_or_md(am_var('file')); else ;
		return true;
	} else if (am_var('html-file')) {
		renderAny(am_var('html-file'));
		return true;
	}

	return false;
}

function footer_menu() {
	echo replace_vars(am_var('footerMessage'));
}

function before_file() {
	echo '<hr class="above-header-content" />' . am_var('nl');
	echo '<div id="inner-content" class="content container node-' . am_var('node') . ' site-' . am_var('safeName') . '">';
}

function after_file() {
	if (am_var('local')) seo_info();
	echo '</div>';
}

function site_humanize($txt, $field = 'title') {
	$pages = [
		'2022' => 'LFL 2022 Archive',
	];

	if (array_key_exists($key = strtolower($txt), $pages))
		return $pages[$key];

	return $txt;
}

?>
