<ul class="nav-menu">
<?php
echo '<li class="drop-down"><a>' . am_var('name') . '</a>';
menu('/content/', ['home-link-to-section' => true, 'exclude-files' => ['home', 'index-kg']]);
echo '</li>';

$items = get_featured_pages();

echo '<li class="drop-down"><a>Featured</a>';
menu('/content/', ['files' => $items]);
echo '</li>';
?>
</ul>

