<?php
am_var('live', $live = endsWith(SITEPATH,'-live'));
am_var('local', $local = $_SERVER['HTTP_HOST'] ==='localhost');

include_once 'functions.php';

bootstrap(array(
	'name' => 'IronbarkFarm.org',
	'safeName' => $safeName = 'ironbarkfarm',
	'byline' => 'A Kindom Village',
	'footerMessage' => '<a class="emphasize" href="%url%kindom/">The living dream of Kindom!</a>',

	'version' => [ 'id' => '1', 'date' => '25 Jul 2022' ],
	
	'sections' => ['archives', 'areas'],

	'theme' => 'biz-land',
	'image-in-logo' => '-logo.png',

	'og:image' => '%url%assets/ironbarkfarm-opengraph.png',
	'og:description' => 'Creating a large community of men, women and children who are committed to learning to live without doing harm to nature, gradually withdrawing from our harmful habits as we examine, challenge and change them, all the while creating abundant food forests and sustainable infrastructure.', //TODO: SEO PER PAGE

	'folder' => 'content/',
	'start_year' => 2006,
	'styles' => ['styles'],
	'support_page_parameters' => true,

	'email' => 'action@loveforlife.com.au',
	'phone' => '+61-418203204',

	'social' => [
		['type' => 'facebook', 'link' => 'https://www.facebook.com/ironbarkfarm/'],
		['type' => 'youtube', 'link' => 'https://youtube.com/@ironbarkfarm'],
	],

	'styles' => [
		'styles',
		//'%app%themes/biz-land/assets/vendor/icofont/icofont.min',
	],

	'stripe-direct-url' => $stripeUrl = 'https://donate.stripe.com/eVa7vqcQqgPK7du8ww?__embed_source=buy_btn_1OlhzACH7yCe8F4Lty1IoSg1',

	'uses' => 'search1, footer-menu1, home-slider',

	'contact' => [
		'whatsapp' => '+61-418203204',
		'phone-timings' => '(Arthur Cristian)',
		'email' => 'write-to-us@ironbarkfarm.org',
		'donate-details' => '<a href="' . $stripeUrl . '/" target="_blank">DONATE ON STRIPE</a><br />
(For donations larger than A$1000, we request you to make a direct bank transfer to Fiona\'s ANZ Bank Account - details mentioned below)<hr />
<strong>ANZ Bank Account<br />
Australian ANZ SWIFT BIC Code: ANZBAU3M<br />
Account Name: Fiona Caroline Cristian<br />
BSB (Branch Number): 012 547<br />
Account Number): 5576 81376</strong><br />
<small>and please write Gift because this is not a business/commercial arrangement<br />
Fiona is not selling or buying anything.</small>',
	],

	'url' => $live ? ($local ? 'http://localhost/allsites/ironbarkfarm-org-live/' : '//ironbarkfarm.org/')
			: ($local ? 'http://localhost/allsites/ironbarkfarm-org/' : '//loveforlife.com.au/allsites/ironbarkfarm-org/'),
	'path' => SITEPATH,
	'no-local-stats' => true,
));

render();
?>

